## negs_stats

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gh/ssoerens/negs_stats.git/master)

Statistical review for 2014 Newberry EGS stimulation.
