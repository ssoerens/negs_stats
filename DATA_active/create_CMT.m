% write_CMT

close all
clear all

time_shift = 0.0;
half_duration = 0.0;
T = readtable('mt_2014.xls');
T = T([1:54 56:end],:);
T_F = readtable('mt_all.xls');
% elev = 1700; % elevation (approximate) of well pad in meters
% elev = 0;
% T.Depth = T_F.Depth+elev/1000;
T.Lat = T_F.Lat;
T.Long = T_F.Long;
T.mb = T_F.mw;
T.Ms = T_F.mw;

l = size(T,1);

ename = strcat(num2str(T.Year,'%04u'),num2str(T.Mo,'%02u'),num2str(T.Day,'%02u'),num2str(T.Hr,'%02u'),num2str(T.min,'%02u'),num2str(T.Sec,'%02.0f'));
% ename_str2 = sprintf('%04u%02u%02u%02u%02u',T.Year,T.Mo,T.Day,T.Hr,T.min);
pname = strcat('NEGS_',ename);
% ename = cellstr(ename);
% pname = cellstr(pname);

T.pname = pname;
T.ename = ename;

time_shift(1:l,1) = time_shift;
half_duration(1:l,1) = half_duration;
T.shift = time_shift;
T.half = half_duration;

D = importdata('newb_grid_seis.xyz');
% [X, Y, Z] = meshgrid(D(:,1), D(:,2), D(:,3));
[Z R] =vec2mtx(D(:,2),D(:,1),1e5);
D = importdata('newb_seis.xyz');
Z = imbedm(D(:,2),D(:,1),D(:,3),Z,R);
elev = ltln2val(Z,R,T.Lat,T.Long);
T.Depth = T_F.Depth+elev/1000;

cmt = fopen('CMTSOLUTION', 'w');

%{ }
Header = 'PDE %u %02u %02u %02u %02u %02.4f %.6f %.6f %.4f %.3f %.3f %s\n';
Name = 'event name:\t%s\n';
Shift = 'time shift: \t%.9f\n';
Half = 'half duration: \t%.9f\n';
Lat = 'latorUTM: \t%.6f\n';
Long = 'longorUTM: \t%.6f\n';
Depth = 'Depth: \t%.4f\n';
Mmm = 'Mrr: \t%.4e\nMtt: \t%.4e\nMpp: \t%.4e\nMrt: \t%.4e\nMrp: \t%.4e\nMtp: \t%.4e\n';

for i=1:l
cmt_name = sprintf('CMTSOLUTION_%i', i);
cmt = fopen(cmt_name, 'w');
fprintf(cmt,Header,T.Year(i), T.Mo(i), T.Day(i), T.Hr(i), T.min(i), T.Sec(i), T.Lat(i), T.Long(i), T.Depth(i), T.mb(i), T.Ms(i), T.pname(i,:));
fprintf(cmt,Name,T.ename(i,:));
fprintf(cmt,Shift,T.shift(i));
fprintf(cmt,Half,T.half(i));
fprintf(cmt,Lat,T.Lat(i));
fprintf(cmt,Long,T.Long(i));
fprintf(cmt,Depth,T.Depth(i));
fprintf(cmt,Mmm,T.NN(i),T.EE(i),T.DD(i),T.NE(i),T.ND(i),T.ED(i));
end




% 'PDE' T.Year T.Mo T.Day T.Hr T.min T.Sec Lat Lon Depth mb Ms ename
% event name: ename
% time shift:      0.0000
% half duration:   0.000
% latorUTM:       43.7263
% longorUTM:     -121.31
% depth:           2.8
% Mrr:       5.30e-2
% Mtt:      -1.17e-1
% Mpp:       2.21e-1
% Mrt:       6.78e-2
% Mrp:       1.62e-1
% Mtp:       2.21e-1
%}