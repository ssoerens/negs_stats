% concatenate grids

close all
clearvars

read_raw = 0;
write = 0;
cut = 0;
grid_topo = 1;

top

range = [ -121.35 ... % Lon min
          43.689; ... % Lat min
          -121.2735 ... % Lon max
          43.766 ... % Lat max
          ];

if read_raw==1
    textfiles = dir('../**/*/job337538_43121_*_raw.txt');
    % D = importdata(textfiles);
    G = zeros(1,3);
    l = 0;
    k = 0;
    for i = 1:length(textfiles)
        file = textfiles(i).name;
        D = importdata(file);
        s = length(D.data);
        l = l + s;
        k = k + 1;
        G(k:l,:) = D.data;
        k = k + s - 1;
    end
    clear D
    
    if cut==1
        ind_lon = G(:,1)>range(1) & G(:,1)<range(2);
        ind_lat = G(:,2)>range(3) & G(:,2)<range(4);
        ind = ind_lon & ind_lat;
        G = G(ind,:);
        clear ind
    end
    
    G = sortrows(G,1);
    G = sortrows(G,2);
    
    lon = [min(G(:,1)) max(G(:,1))];
    lat = [min(G(:,2)) max(G(:,2))];
    elev = [min(G(:,3)) max(G(:,3))];
    
    if write==1
        xyz = fopen('Newberry_topo.xyz','w');
        fprintf(xyz,'%.7f \t%.7f \t%.2f\n',G');
        
        %     topo = fopen('topo_fine.dat','w');
        %     fprintf(topo,'%.2f\n',G(:,3));
    end
end

if grid_topo==1
    D = importdata('newb_surface.xyz');
    D = sortrows(D,[2,1]);
    z = D(:,3);
    topo = fopen('topo_fine_surface.dat','w');
    fprintf(topo,'%.2f\n',z);
end
% max_lat = max(
